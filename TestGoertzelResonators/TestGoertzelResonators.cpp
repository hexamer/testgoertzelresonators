﻿//////////////////////////////////////////////////////////////////////////////////////////////////
//
// TestGoertzelResonators
//
// Copyright (c) 2016, 2017 by Aaron Hexamer
//
// This file is part of the TestGoertzelResonators program.
//
// TestGoertzelResonators is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// TestGoertzelResonators is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with TestGoertzelResonators.  If not, see <http://www.gnu.org/licenses/>.
//
// Module TestGoertzelResonators.cpp: Main module for the application.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <intrin.h>
#include <stdint.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <complex.h>
#include <fftw3.h>
#include <vector>
#include <complex>
#include <array>
#include <time.h>

namespace po = boost::program_options;

typedef enum
{
    BIQUAD_GOERTZEL,
    WAVEGUIDE,
    MAGIC_CIRCLE,
    STAGGERED_2,
    COUPLED,
    REINSCH_TURNER,    // Reinsch as published in Clay Turner's Resonators Table
    REINSCH_0,         // Reinsch as published in Gentleman/Stoer/Bulirsch/Oliver
    REINSCH_PI,        // Reinsch(pi) as published in Gentleman/Stoer/Bulirsch/Oliver
    TYPE_A,
    TYPE_B,
    TYPE_C,
    TYPE_C_MODIFIED,
    MAX_RESONATOR_TYPE
} RESONATOR_T;

RESONATOR_T cfgResType;
bool cfgOutputPerformance = false;
bool cfgRunCoarse = false;
bool cfgRunFine = false;
uint32_t cfgMaxN; // Must be a power of 2
uint32_t cfgMinN = 1; // Must be a power of 2
uint32_t cfgNumCoarseGoertzelSamples; // Must be a power of 2
uint32_t cfgIter = 10; // default to 10 in case the user does not care to specify
uint32_t cfgNumFineGoertzelSamples; // Must be a multiple of 2
bool cfgUseFMA3 = false; // Use FMA3 instructions

void RunFFT(uint32_t N);
void TestCoarse(uint32_t N);
void TestFine(uint32_t N);
void ComputeGoertzelMatrix(double* theta);
void Goertzel(uint32_t N, double* theta, std::complex<double> *GoertzelOutput);
void Setup(uint32_t maxN);
void Generate(uint32_t maxN);
void CleanUp(void);

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
    LARGE_INTEGER li;
    if (!QueryPerformanceFrequency(&li))
    {
        std::cout << "QueryPerformanceFrequency failed!\n";
    }

    PCFreq = double(li.QuadPart) / 1000.0;

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;
}
double GetCounter()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return double(li.QuadPart - CounterStart) / PCFreq;
}

double* in;
fftw_complex* out;
std::vector<std::complex<double>> GoertzelOutputs;
std::vector<double> GoertzelLogSpreadCoarseErrors;
std::vector<double> GoertzelLogSpreadCoarseErrorsAvg;
std::vector<std::vector<double>> GoertzelLogErrorGrowthWC;
std::vector<double> GoertzelLogFineErrors;
std::vector<double> GoertzelLogFineErrorsWC;

int main(int argc, char* argv[])
{
    // Parse command line options

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("resType", po::value<int>(), "resonator type as an index")
        ("perf", "output performance data")
        ("coarse", po::value<int>(), "run arg coarse samples, 2^arg")
        ("fine", po::value<int>(), "run arg fine samples, arg multiple of 2")
        ("minN", po::value<int>(), "minimum N, 2^arg")
        ("maxN", po::value<int>(), "maximum N, 2^arg")
        ("iter", po::value<int>(), "number of iterations (to average for coarse, to find w/c for fine)")
        ("fma3", "use FMA3 instructions");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    }
    catch (const std::exception &ex)
    {
        UNREFERENCED_PARAMETER(ex);
        std::cout << "Illegal command line syntax.\n";
        std::cout << desc << "\n";
        return -1;
    }

    if (vm.count("help"))
    {
        std::cout << desc << "\n";
        return 0;
    }

    if (vm.count("resType"))
    {
        if (vm["resType"].as<int>() < MAX_RESONATOR_TYPE && vm["resType"].as<int>() >= 0)
        {
            cfgResType = (RESONATOR_T)vm["resType"].as<int>();
        }
        else
        {
            std::cout << "resType argument must be an integer between 0 and " << MAX_RESONATOR_TYPE - 1 << " " << "inclusive\n";
            return 1;
        }
    }
    else
    {
        std::cout << "resType argument must be specified\n";
        return 1;
    }

    if (vm.count("perf"))
    {
        cfgOutputPerformance = true;
    }
    else
    {
        cfgOutputPerformance = false;
    }

    if (vm.count("coarse"))
    {
        if (vm["coarse"].as<int>() <= 31 && vm["coarse"].as<int>() >= 0)
        {
            cfgNumCoarseGoertzelSamples = 1 << vm["coarse"].as<int>();
            cfgRunCoarse = true;
        }
        else
        {
            std::cout << "coarse argument must be an integer between 0 and 31 inclusive\n";
            return 1;
        }
    }

    if (vm.count("fine"))
    {
        if (0 == vm["fine"].as<int>() % 2 && vm["fine"].as<int>() > 0)
        {
            cfgNumFineGoertzelSamples = vm["fine"].as<int>();
            cfgRunFine = true;
        }
        else
        {
            std::cout << "fine argument must be a positive even integer\n";
            return 1;
        }
    }

    if (!(cfgRunCoarse || cfgRunFine))
    {
        std::cout << "nothing to do, need to specify coarse and/or fine\n";
        return 1;
    }

    if (vm.count("maxN"))
    {
        if (vm["maxN"].as<int>() <= 31 && vm["maxN"].as<int>() >= 0)
        {
            cfgMaxN = 1 << vm["maxN"].as<int>();
        }
        else
        {
            std::cout << "maxN argument must be an integer between 0 and 31 inclusive\n";
            return 1;
        }
    }
    else
    {
        std::cout << "maxN argument must be specified\n";
        return 1;
    }

    if (vm.count("minN"))
    {
        if (vm["minN"].as<int>() <= 31 && vm["minN"].as<int>() >= 0)
        {
            cfgMinN = 1 << vm["minN"].as<int>();
        }
        else
        {
            std::cout << "minN argument must be an integer between 0 and 31 inclusive\n";
            return 1;
        }
    }

    if (cfgMinN > cfgMaxN)
    {
        std::cout << "minN must be <= maxN\n";
        return 1;
    }

    if (cfgNumCoarseGoertzelSamples >= cfgMinN)
    {
        std::cout << "coarse argument must be < minN\n";
        return 1;
    }

    if (vm.count("iter"))
    {
        if (vm["iter"].as<int>() > 0)
        {
            cfgIter = vm["iter"].as<int>();
        }
        else
        {
            std::cout << "iter argument must be an integer >= 1\n";
            return 1;
        }
    }

    if (vm.count("fma3"))
    {
        cfgUseFMA3 = true;
    }
    else
    {
        cfgUseFMA3 = false;
    }

    Setup(cfgMaxN);

    uint32_t growthIndex = 0;

    // Sample size (N) loop
    for (uint32_t N = cfgMinN; N <= cfgMaxN; N <<= 1)
    {
        // Clear out averages
        for (uint32_t i = 0; i <= cfgNumCoarseGoertzelSamples; i++)
        {
            GoertzelLogSpreadCoarseErrorsAvg[i] = 0.0;
        }
        // Initialize worst cases
        for (uint32_t i = 0; i < 3; i++)
        {
            GoertzelLogFineErrorsWC[i] = -100.0;
        }
        // Iterations sample loop
        for (uint32_t i = 0; i < cfgIter; i++)
        {
            Generate(N);
            RunFFT(N);
            if (cfgRunCoarse)
            {
                TestCoarse(N);
                for (uint32_t j = 0; j <= cfgNumCoarseGoertzelSamples; j++)
                {
                    GoertzelLogSpreadCoarseErrorsAvg[j] += GoertzelLogSpreadCoarseErrors[j];
                }
            }
            if (cfgRunFine)
            {
                TestFine(N);
                for (uint32_t j = 0; j < 3; j++)
                {
                    GoertzelLogFineErrorsWC[j] = max(GoertzelLogFineErrorsWC[j], GoertzelLogFineErrors[j]);
                }
            }
        }
        // Compute averages
        for (uint32_t i = 0; i <= cfgNumCoarseGoertzelSamples; i++)
        {
            GoertzelLogSpreadCoarseErrorsAvg[i] /= cfgIter;
        }

        // Store fine worst cases for error growth
        GoertzelLogErrorGrowthWC[0][growthIndex] = GoertzelLogFineErrorsWC[0];
        GoertzelLogErrorGrowthWC[1][growthIndex] = GoertzelLogFineErrorsWC[1];
        GoertzelLogErrorGrowthWC[2][growthIndex] = GoertzelLogFineErrorsWC[2];
        growthIndex++;

        if (cfgRunCoarse)
        {
            std::cout << "f, log(err) N = " << N << "\n";
            for (uint32_t i = 0; i <= cfgNumCoarseGoertzelSamples; i++)
            {
                std::cout << 0.5 * (double)i / (double)cfgNumCoarseGoertzelSamples << ", " << GoertzelLogSpreadCoarseErrorsAvg[i] << "\n";
            }
            std::cout << "\n";
        }
    }

    // Print growth vectors

    if (cfgRunFine)
    {
        growthIndex = 0;
        std::cout << "N, log(err) theta ~ 0, log(err) theta ~ pi/2, log(err) theta ~ pi\n";
        for (uint32_t N = cfgMinN; N <= cfgMaxN; N <<= 1)
        {
            std::cout << N << ", " << GoertzelLogErrorGrowthWC[0][growthIndex] << ", " <<
                GoertzelLogErrorGrowthWC[1][growthIndex] << ", " <<
                GoertzelLogErrorGrowthWC[2][growthIndex] << "\n";
            growthIndex++;
        }
    }

    return 0;
}

void RunFFT(uint32_t N)
{
    fftw_plan p;
    double timerMs;

    // Must use ESTIMATE since we are relying on the plan not destroying the input data.
    p = fftw_plan_dft_r2c_1d(N, in, out, FFTW_ESTIMATE);

    StartCounter();
    fftw_execute_dft_r2c(p, in, out);
    timerMs = GetCounter();

    fftw_destroy_plan(p);

    if (cfgOutputPerformance)
    {
        std::cout << "For N = " << N << ": FFT elapsed time = " << timerMs << " ms.\n\n";
    }
}

void TestCoarse(uint32_t N)
{
    double timerMs;

    // Work on coarse samples

    timerMs = 0.0;
    for (uint32_t i = 0; i <= cfgNumCoarseGoertzelSamples; i += 2)
    {
        std::complex<double> output[2];
        double theta[2];

        // Spread the samples over 0 to Fs/2 (0 to PI)
        StartCounter();
        theta[0] = M_PI * (double)i / (double)cfgNumCoarseGoertzelSamples;
        theta[1] = M_PI * (double)(i + 1) / (double)cfgNumCoarseGoertzelSamples;
        Goertzel(N, theta, output);
        timerMs += GetCounter();
        GoertzelOutputs[i] = output[0];
        GoertzelOutputs[i + 1] = output[1];
    }

    timerMs /= cfgNumCoarseGoertzelSamples;
    if (cfgOutputPerformance)
    {
        std::cout << "For N = " << N << ": Avg. Goertzel elapsed time = " << timerMs << " ms.\n\n";
    }

    // Compare Goertzel to FFT
    uint32_t sampDist = N / 2 / cfgNumCoarseGoertzelSamples;
    for (uint32_t i = 0; i <= cfgNumCoarseGoertzelSamples; i++)
    {
        std::complex<double> FFTSamp(out[i*sampDist][0], out[i*sampDist][1]);
        GoertzelLogSpreadCoarseErrors[i] = log10(fabs((abs(GoertzelOutputs[i]) - abs(FFTSamp)) / abs(FFTSamp)));
    }
}

void TestFine(uint32_t N)
{

    // Work on fine samples for error growth
    // Near theta = 0
    GoertzelLogFineErrors[0] = -100.0;
    for (uint32_t i = 0; i < cfgNumFineGoertzelSamples; i += 2)
    {
        std::complex<double> output[2];
        double theta[2];

        theta[0] = M_PI * (double)i / (double)(N/2);
        theta[1] = M_PI * (double)(i+1) / (double)(N/2);
        Goertzel(N, theta, output);
        
        std::complex<double> FFTSamp;

        FFTSamp = std::complex<double>(out[i][0], out[i][1]);
        GoertzelLogFineErrors[0] = max(GoertzelLogFineErrors[0], log10(fabs((abs(output[0]) - abs(FFTSamp)) / abs(FFTSamp))));

        FFTSamp = std::complex<double>(out[i+1][0], out[i+1][1]);
        GoertzelLogFineErrors[0] = max(GoertzelLogFineErrors[0], log10(fabs((abs(output[1]) - abs(FFTSamp)) / abs(FFTSamp))));
    }

    // Near theta = pi/2
    GoertzelLogFineErrors[1] = -100.0;
    for (uint32_t i = N/4-cfgNumFineGoertzelSamples/2; i < N/4+cfgNumFineGoertzelSamples/2; i += 2)
    {
        std::complex<double> output[2];
        double theta[2];

        theta[0] = M_PI * (double)i / (double)(N/2);
        theta[1] = M_PI * (double)(i+1) / (double)(N/2);
        Goertzel(N, theta, output);

        std::complex<double> FFTSamp;

        FFTSamp = std::complex<double>(out[i][0], out[i][1]);
        GoertzelLogFineErrors[1] = max(GoertzelLogFineErrors[1], log10(fabs((abs(output[0]) - abs(FFTSamp)) / abs(FFTSamp))));

        FFTSamp = std::complex<double>(out[i + 1][0], out[i + 1][1]);
        GoertzelLogFineErrors[1] = max(GoertzelLogFineErrors[1], log10(fabs((abs(output[1]) - abs(FFTSamp)) / abs(FFTSamp))));
    }

    // Near theta = pi
    GoertzelLogFineErrors[2] = -100.0;
    for (uint32_t i = N/2 - cfgNumFineGoertzelSamples + 1; i <= N/2; i += 2)
    {
        std::complex<double> output[2];
        double theta[2];

        theta[0] = M_PI * (double)i / (double)(N/2);
        theta[1] = M_PI * (double)(i+1) / (double)(N/2);
        Goertzel(N, theta, output);

        std::complex<double> FFTSamp;

        FFTSamp = std::complex<double>(out[i][0], out[i][1]);
        GoertzelLogFineErrors[2] = max(GoertzelLogFineErrors[2], log10(fabs((abs(output[0]) - abs(FFTSamp)) / abs(FFTSamp))));

        FFTSamp = std::complex<double>(out[i + 1][0], out[i + 1][1]);
        GoertzelLogFineErrors[2] = max(GoertzelLogFineErrors[2], log10(fabs((abs(output[1]) - abs(FFTSamp)) / abs(FFTSamp))));
    }
}

// Goertzel variables
alignas(32) double α11[2], α12[2], α21[2], α22[2];
alignas(32) double λ11[2], λ12[2], λ21[2], λ22[2];
alignas(32) std::array<double,2> kappa;
alignas(32) double cs[4];

void ComputeGoertzelMatrix(double* theta)
{
    switch (cfgResType)
    {
        case BIQUAD_GOERTZEL:
            α11[0] = 2.0 * cos(theta[0]);
            α11[1] = 2.0 * cos(theta[1]);
            α12[0] = -1.0;
            α12[1] = -1.0;
            α21[0] = 1.0;
            α21[1] = 1.0;
            α22[0] = 0.0;
            α22[1] = 0.0;

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = -cos(theta[0]);
            λ12[1] = -cos(theta[1]);
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = -sin(theta[0]);
            λ22[1] = -sin(theta[1]);
            break;
        case WAVEGUIDE:
            α11[0] = cos(theta[0]);
            α11[1] = cos(theta[1]);
            α12[0] = cos(theta[0]) - 1.0;
            α12[1] = cos(theta[1]) - 1.0;
            α21[0] = cos(theta[0]) + 1.0;
            α21[1] = cos(theta[1]) + 1.0;
            α22[0] = cos(theta[0]);
            α22[1] = cos(theta[1]);

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = 0.0;
            λ12[1] = 0.0;
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = -tan(theta[0] / 2.0);
            λ22[1] = -tan(theta[1] / 2.0);
            break;
        case MAGIC_CIRCLE:
        {
            kappa = { 2.0 * sin(theta[0] / 2.0), 2.0 * sin(theta[1] / 2.0) };
            α11[0] = 1.0 - kappa[0] * kappa[0];
            α11[1] = 1.0 - kappa[1] * kappa[1];
            α12[0] = kappa[0];
            α12[1] = kappa[1];
            α21[0] = -kappa[0];
            α21[1] = -kappa[1];
            α22[0] = 1.0;
            α22[1] = 1.0;

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = -sin(theta[0] / 2.0);
            λ12[1] = -sin(theta[1] / 2.0);
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = cos(theta[0] / 2.0);
            λ22[1] = cos(theta[1] / 2.0);
            break;
        }
        case STAGGERED_2:
        {
            kappa = { cos(theta[0]), cos(theta[1]) };
            α11[0] = kappa[0];
            α11[1] = kappa[1];
            α12[0] = 1.0 - kappa[0] * kappa[0];
            α12[1] = 1.0 - kappa[1] * kappa[1];
            α21[0] = -1.0;
            α21[1] = -1.0;
            α22[0] = kappa[0];
            α22[1] = kappa[1];

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = 0.0;
            λ12[1] = 0.0;
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = sin(theta[0]);
            λ22[1] = sin(theta[1]);
            break;
        }
        case COUPLED:
        {
            kappa = { sin(theta[0]), sin(theta[1]) };
            α11[0] = sqrt(1.0 - kappa[0] * kappa[0]);
            α11[1] = sqrt(1.0 - kappa[1] * kappa[1]);
            α12[0] = kappa[0];
            α12[1] = kappa[1];
            α21[0] = -kappa[0];
            α21[1] = -kappa[1];
            α22[0] = α11[0];
            α22[1] = α11[1];

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = 0.0;
            λ12[1] = 0.0;
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = 1.0;
            λ22[1] = 1.0;
            break;
        }
        case REINSCH_TURNER:
        {
            kappa = { 2.0 * cos(theta[0]) - 1.0, 2.0 * cos(theta[1]) - 1.0 };
            α11[0] = kappa[0];
            α11[1] = kappa[1];
            α12[0] = 1.0;
            α12[1] = 1.0;
            α21[0] = kappa[0] - 1.0;
            α21[1] = kappa[1] - 1.0;
            α22[0] = 1.0;
            α22[1] = 1.0;

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = -0.5;
            λ12[1] = -0.5;
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = 0.5 / tan(theta[0] / 2.0);
            λ22[1] = 0.5 / tan(theta[1] / 2.0);
            break;
        }
        case REINSCH_0:
        {
            kappa = { -4.0 * pow(sin(theta[0]/2.0), 2.0), -4.0 * pow(sin(theta[1]/2.0), 2.0) };

            λ11[0] = kappa[0] / 2.0;
            λ11[1] = kappa[1] / 2.0;
            λ12[0] = 1.0;
            λ12[1] = 1.0;
            λ21[0] = sin(theta[0]);
            λ21[1] = sin(theta[1]);
            λ22[0] = 0.0;
            λ22[1] = 0.0;

            break;
        }
        case REINSCH_PI:
        {
            kappa = { 4.0 * pow(cos(theta[0] / 2.0), 2.0), 4.0 * pow(cos(theta[1] / 2.0), 2.0) };

            λ11[0] = -kappa[0] / 2.0;
            λ11[1] = -kappa[1] / 2.0;
            λ12[0] = 1.0;
            λ12[1] = 1.0;
            λ21[0] = sin(theta[0]);
            λ21[1] = sin(theta[1]);
            λ22[0] = 0.0;
            λ22[1] = 0.0;

            break;
        }
        case TYPE_A:
        {
            kappa = { 4.0 * pow(sin(theta[0] / 2.0), 2.0), 4.0 * pow(sin(theta[1] / 2.0), 2.0) };

            α11[0] = 1.0 - kappa[0];
            α11[1] = 1.0 - kappa[1];
            α12[0] = -kappa[0];
            α12[1] = -kappa[1];
            α21[0] = 1.0;
            α21[1] = 1.0;
            α22[0] = 1.0;
            α22[1] = 1.0;

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = kappa[0] / 2.0;
            λ12[1] = kappa[1] / 2.0;
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = -sin(theta[0]);
            λ22[1] = -sin(theta[1]);
        }
        case TYPE_B:
        {
            α11[0] = 0.0;
            α11[1] = 0.0;
            α12[0] = 1.0;
            α12[1] = 1.0;
            α21[0] = -1.0;
            α21[1] = -1.0;
            α22[0] = 2.0 * cos(theta[0]);
            α22[1] = 2.0 * cos(theta[1]);

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = cos(theta[0]);
            λ12[1] = cos(theta[1]);
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = sin(theta[0]);
            λ22[1] = sin(theta[1]);
        }
        case TYPE_C:
        {
            kappa = { 4.0 * pow(cos(theta[0] / 2.0), 2.0), 4.0 * pow(cos(theta[1] / 2.0), 2.0) };

            α11[0] = kappa[0] - 1.0;
            α11[1] = kappa[1] - 1.0;
            α12[0] = kappa[0];
            α12[1] = kappa[1];
            α21[0] = -1.0;
            α21[1] = -1.0;
            α22[0] = -1.0;
            α22[1] = -1.0;

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = kappa[0] / 2.0;
            λ12[1] = kappa[1] / 2.0;
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = sin(theta[0]);
            λ22[1] = sin(theta[1]);
        }
        case TYPE_C_MODIFIED:
        {
            kappa = { -4.0 * pow(sin(theta[0] / 2.0), 2.0), -4.0 * pow(sin(theta[1] / 2.0), 2.0) };

            α11[0] = kappa[0] + 1.0;
            α11[1] = kappa[1] + 1.0;
            α12[0] = kappa[0];
            α12[1] = kappa[1];
            α21[0] = 1.0;
            α21[1] = 1.0;
            α22[0] = 1.0;
            α22[1] = 1.0;

            λ11[0] = 1.0;
            λ11[1] = 1.0;
            λ12[0] = -kappa[0] / 2.0;
            λ12[1] = -kappa[1] / 2.0;
            λ21[0] = 0.0;
            λ21[1] = 0.0;
            λ22[0] = -sin(theta[0]);
            λ22[1] = -sin(theta[1]);
        }
        default:
            break;
    }
}

void Goertzel(uint32_t N, double* theta, std::complex<double> *GoertzelOutput)
{
    ComputeGoertzelMatrix(theta);

    if (cfgResType != REINSCH_0 && cfgResType != REINSCH_PI)
    {
        // Vectors
        __m256d aVec, bVec, abPrimeVec, αCol1, αCol2, λCol1, λCol2, sampleVec, csVec;

        // aPrime_0 = α11_0*a_0 + α12_0*b_0 + samp
        // bPrime_0 = α21_0*a_0 + α22_0*b_0
        // aPrime_1 = α11_1*a_1 + α12_1*b_1 + samp
        // bPrime_1 = α21_1*a_1 + α22_1*b_1

        aVec = _mm256_set1_pd(0.0);
        bVec = _mm256_set1_pd(0.0);

        αCol1 = _mm256_set_pd(α11[0], α21[0], α11[1], α21[1]);
        αCol2 = _mm256_set_pd(α12[0], α22[0], α12[1], α22[1]);

        if (cfgUseFMA3)
        {
            for (uint32_t i = 0; i < N; i++)
            {
                sampleVec = _mm256_set_pd(in[i], 0.0, in[i], 0.0);
                abPrimeVec = _mm256_fmadd_pd(αCol1, aVec, _mm256_fmadd_pd(αCol2, bVec, sampleVec));
                aVec = _mm256_permute_pd(abPrimeVec, 0xf);
                bVec = _mm256_permute_pd(abPrimeVec, 0);
            }
        }
        else
        {
            for (uint32_t i = 0; i < N; i++)
            {
                sampleVec = _mm256_set_pd(in[i], 0.0, in[i], 0.0);
                abPrimeVec = _mm256_add_pd(_mm256_add_pd(_mm256_mul_pd(αCol1, aVec), _mm256_mul_pd(αCol2, bVec)), sampleVec);
                aVec = _mm256_permute_pd(abPrimeVec, 0xf);
                bVec = _mm256_permute_pd(abPrimeVec, 0);
            }
        }

        // Run once more with 0 sample to get correct phase
        abPrimeVec = _mm256_add_pd(_mm256_mul_pd(αCol1, aVec), _mm256_mul_pd(αCol2, bVec));
        aVec = _mm256_permute_pd(abPrimeVec, 0xf);
        bVec = _mm256_permute_pd(abPrimeVec, 0);

        // Compute complex output
        λCol1 = _mm256_set_pd(λ11[0], λ21[0], λ11[1], λ21[1]);
        λCol2 = _mm256_set_pd(λ12[0], λ22[0], λ12[1], λ22[1]);

        csVec = _mm256_add_pd(_mm256_mul_pd(λCol1, aVec), _mm256_mul_pd(λCol2, bVec));
        // unload vector
        _mm256_store_pd(cs, csVec);
    }
    else
    {
        __m128d kappaVec, aVec, bVec, sampleVec;
        alignas(16) double a[2], b[2];
        
        aVec = _mm_set1_pd(0.0);
        bVec = _mm_set1_pd(0.0);
        kappaVec = _mm_set_pd(kappa[1], kappa[0]);

        if (cfgResType == REINSCH_0)
        {
            if (cfgUseFMA3)
            {
                for (uint32_t i = 0; i < N; i++)
                {
                    sampleVec = _mm_set1_pd(in[i]);
                    //b = b + K*a + samples[i]
                    //a = a + b
                    bVec = _mm_add_pd(_mm_fmadd_pd(kappaVec, aVec, bVec), sampleVec);
                    aVec = _mm_add_pd(aVec, bVec);
                }
            }
            else
            {
                for (uint32_t i = 0; i < N; i++)
                {
                    sampleVec = _mm_set1_pd(in[i]);
                    //b = b + K*a + samples[i]
                    //a = a + b
                    bVec = _mm_add_pd(_mm_add_pd(bVec, _mm_mul_pd(kappaVec, aVec)), sampleVec);
                    aVec = _mm_add_pd(aVec, bVec);
                }
            }

            // Run once more with 0 sample to get correct phase
            bVec = _mm_add_pd(bVec, _mm_mul_pd(kappaVec, aVec));
            aVec = _mm_add_pd(aVec, bVec);
        }
        else // REINSCH_PI
        {
            if (cfgUseFMA3)
            {
                for (uint32_t i = 0; i < N; i++)
                {
                    sampleVec = _mm_set1_pd(in[i]);
                    //b = K*a - b + samples[i]
                    //a = b - a
                    bVec = _mm_add_pd(_mm_fmsub_pd(kappaVec, aVec, bVec), sampleVec);
                    aVec = _mm_sub_pd(bVec, aVec);
                }
            }
            else
            {
                for (uint32_t i = 0; i < N; i++)
                {
                    sampleVec = _mm_set1_pd(in[i]);
                    //b = K*a - b + samples[i]
                    //a = b - a
                    bVec = _mm_add_pd(_mm_sub_pd(_mm_mul_pd(kappaVec, aVec), bVec), sampleVec);
                    aVec = _mm_sub_pd(bVec, aVec);
                }
            }

            // Run once more with 0 sample to get correct phase
            bVec = _mm_sub_pd(_mm_mul_pd(kappaVec, aVec), bVec);
            aVec = _mm_sub_pd(bVec, aVec);
        }

        _mm_store_pd(a, aVec);
        _mm_store_pd(b, bVec);

        cs[3] = λ11[0] * a[0] + λ12[0] * b[0];
        cs[2] = λ21[0] * a[0] + λ22[0] * b[0];
        cs[1] = λ11[1] * a[1] + λ12[1] * b[1];
        cs[0] = λ21[1] * a[1] + λ22[1] * b[1];
    }

    GoertzelOutput[0] = std::complex<double>(cs[3], cs[2]);
    GoertzelOutput[1] = std::complex<double>(cs[1], cs[0]);
}

void Setup(uint32_t maxN)
{
    in = (double*)fftw_malloc(sizeof(double) * maxN);
    out = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * (maxN/2 + 1));

    GoertzelOutputs.resize(cfgNumCoarseGoertzelSamples+2); // +2 because we do 2 Goertzels at a time and have an odd number of actual samples
    GoertzelLogSpreadCoarseErrors.resize(cfgNumCoarseGoertzelSamples + 1);
    GoertzelLogSpreadCoarseErrorsAvg.resize(cfgNumCoarseGoertzelSamples + 1);
    GoertzelLogFineErrors.resize(3); // One for 0, pi/2, and pi
    GoertzelLogFineErrorsWC.resize(3); // One for 0, pi/2, and pi
    GoertzelLogErrorGrowthWC.resize(3); // One for 0, pi/2, and pi

    uint32_t errorGrowthVectorSize;
    // integer log2
    _BitScanReverse((DWORD*)&errorGrowthVectorSize, cfgMaxN / cfgMinN);
    for (uint32_t i = 0; i < 3; i++)
    {
        GoertzelLogErrorGrowthWC[i].resize(errorGrowthVectorSize+1);
    }
}

void Generate(uint32_t N)
{
    double timerMs;
    StartCounter();
    srand((unsigned int)time(NULL));
    for (uint32_t i = 0; i < N; i++)
    {
        in[i] = (double)rand() / (double)RAND_MAX;
    }
    timerMs = GetCounter();
    if (cfgOutputPerformance)
    {
        std::cout << "For N = " << N << ": Generate elapsed time = " << timerMs << " ms.\n\n";
    }
}

void CleanUp(void)
{
    fftw_free(in);
    fftw_free(out);
}
